<?php
/**
 * Copyright (c) 2013 BUI Family.
 *
 * This file is part of OpenFISMA.
 *
 * OpenFISMA is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * OpenFISMA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with OpenFISMA.  If not, see
 * {@link http://www.gnu.org/licenses/}.
 */

/**
 * The dashboard controller displays the user dashboard when the user first logs
 * in. This controller also produces graphical charts in conjunction with the SWF Charts
 * package.
 *
 * @author     Duy K. Bui <duy.bui@family.info>
 * @copyright  (c) BUI Family. 2013 {@link http://buifamily.info}
 * @license    http://www.openfisma.org/content/license GPLv3
 * @package    Controller
 */
class FirstInstallController extends Fisma_Zend_Controller_Action_Security
{
    /**
     * Hook into the pre-dispatch to do an ACL check
     */
    public function preDispatch()
    {
        parent::preDispatch();

        if (CurrentUser::getAttribute('username') !== 'root') {
            throw new Fisma_Zend_Exception_User("Only root can perform this action.");
        }
    }

    /**
     * Show first installation guide
     *
     * @GETAllowed
     */
    public function indexAction()
    {
        Fisma::configuration()->setConfig('first_install', true);
        $this->view->systemName = Fisma::configuration()->getConfig('system_name');
        $this->view->toolbarButtons = array(
            new Fisma_Yui_Form_Button(
                'completeButton',
                array(
                    'label' => 'Complete',
                    'onClickFunction' => 'Fisma.Util.formPostAction',
                    'onClickArgument' => array(
                        'action' => '/first-install/skip'
                    ),
                    'icon' => 'ok-sign'
                )
            ),
            new Fisma_Yui_Form_Button(
                'skipButton',
                array(
                    'label' => 'Skip',
                    'onClickFunction' => 'Fisma.Util.formPostAction',
                    'onClickArgument' => array(
                        'action' => '/first-install/skip'
                    ),
                    'icon' => 'forward'
                )
            )
        );
    }

    /**
     * Skip first installation guide
     */
    public function skipAction()
    {
        Fisma::configuration()->setConfig('first_install', false);
        $this->_redirect('/');
    }
}
